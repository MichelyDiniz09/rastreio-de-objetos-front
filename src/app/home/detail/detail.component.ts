import { Component, OnInit, Input } from '@angular/core';
import { Delivery } from '../../shared/classes/delivery';
import { DeliveryService } from '../../shared/services/delivery.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.less']
})
export class DetailComponent implements OnInit {

  public delivery: Array<Delivery> = [];
  router: Router;
  codeTrack: String;

  constructor(
    private DeliveryService: DeliveryService, 
    router: Router,
    private route: ActivatedRoute,
    ) { 
    this.router = router;
  }

  ngOnInit() {

    this.getUrlParameter();
    this.DeliveryService.all(this.codeTrack).subscribe(data => {
      this.delivery = data;      
    });
    
  }

  getUrlParameter (){
    this.route.params.subscribe(params => {this.codeTrack = params.codeTrack});
  }

}
