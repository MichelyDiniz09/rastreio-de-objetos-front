import { Component, OnInit, Input } from '@angular/core';
import { Delivery } from '../shared/classes/delivery';
import { DeliveryService } from '../shared/services/delivery.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  public track: Array<Delivery> = [];
  router: Router;

  constructor(private DeliveryService: DeliveryService, router: Router) { 
    this.router = router;
  }

  ngOnInit() {
  }



}
