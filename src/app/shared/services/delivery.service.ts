import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Delivery } from '../classes/delivery';

@Injectable({
    providedIn: 'root'
})

export class DeliveryService {

    public constructor(private http: HttpClient) {
    }

    all(codeTrack) {
        const url = 'http://localhost:8888/object?track='+codeTrack ;
        return this.http.get<Array<Delivery>>(url);
    }
}