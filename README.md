Instruções.

Para executar a aplicação perfeitamente é necessaário ter configurado localmente
a aplicação Back-End, API e Banco de Dados.
Segue link com instruções de como fazer:
https://bitbucket.org/MichelyDiniz09/rastreio-de-objetos-api/src/master/

Apartir da configuração do Back-End é necessário ter ambiente para o front preparado.
Para isso é necessário instalar NPM ou YARN.
Feito isso é só rodar o comando "npm install" no diretorio raiz do projeto e em seguida 
o comando "ng serve" para iniciar a aplicação.

Com aplicação compilada com sucesso, basta acessar a seguinte URL no seu navegador:
http://localhost:4200/

A tela inicial da aplicação será exibida, pronta para utilização.